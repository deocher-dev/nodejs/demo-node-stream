# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security

## [1.0.0] - 2024-01-22

### Added

- Documentation
  - `doc/streams.md`
- Tests
  - `test/util`
  - `test/000-demo-data-source-async`
  - `test/001-demo-data-source-sync`
  - `test/002-demo-data-store-async`
  - `test/003-demo-data-store-sync`
  - `test/010-demo-writable-stream`
  - `test/020-demo-readable-stream`
  - `test/030-demo-pipe`
- Package assets
  - `.gitignore`
  - `CHANGELOG.md`
  - `LICENSE`
  - `README.md`
  - `TODO.md`
  - `demo-node-stream.js`
  - `package-lock.json`
  - `package.json`

### Changed

### Deprecated

### Removed

### Fixed

### Security
