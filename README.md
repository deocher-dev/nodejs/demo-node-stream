![stability: experimental](https://badgen.net/badge/stability/experimental/orange)
![registry: unpublished](https://badgen.net/badge/registry/unpublished/red)

# demo-node-stream

An example of using the `node:stream` module.

## Description

![node: >=18.16.0](https://badgen.net/badge/node/>=18.16.0)
![npm: >=9.5.1](https://badgen.net/badge/npm/>=9.5.1)
![os: linux](https://badgen.net/badge/os/linux)

The package contains an implementation of custom `Readable` and `Writable`
streams - the `DemoReadable` and `DemoWritable` classes.

Also there are two cathegories of helper classes. The `DemoDataSourceAsync`
and `DemoDataSourceSync` classes, which provide a controlled way to supply data
chunks to readable streams, and the `DemoDataStoreAsync` and `DemoDataStoreSync`
classes, which are dedicated to receive data from the writable streams.

The package itself consists of several test suits. The main ones are:

- [010-demo-writable-stream.js](./test/010-demo-writable-stream.js) - basic usage of `Writable` streams
- [020-demo-readable-stream.js](./test/020-demo-readable-stream.js) - basic usage of `Readable` streams
- [030-demo-pipe.js](./test/030-demo-pipe.js) - piping data from `Readable` to `Writable`

The basic and most important information about the streams is collected in the
[`doc/event-loop.md`](./doc/streams.md) document.

## Installation

> The package is not published yet.

<!--
Local installation:

```bash
npm install demo-node-stream
```

Local dev installation:

```bash
npm install --save-dev demo-node-stream
```

Global installation:

```bash
npm install --global demo-node-stream
```
-->

## Tests

Run the following command from the package root directory to launch the tests:

```bash
npm run test
```

## License

[MIT](./LICENSE)
