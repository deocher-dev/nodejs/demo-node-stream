# Streams

This tutorial consists of excerpts from the official Node.js documentation. The
purpose of this reference is to summarize the crucial information about the
stream API, scattered across multiple resources.

## General Notes

Streams can be readable, writable, or both. All streams are instances of
`EventEmitter`.

There are four fundamental stream types within Node.js:[<sup>Types of streams</sup>][types-of-streams]

- `Writable`: streams to which data can be written.
- `Readable`: streams from which data can be read.
- `Duplex`: streams that are both `Readable` and `Writable`.
- `Transform`: `Duplex` streams that can modify or transform the data as it is
  written and read.

All streams created by Node.js APIs operate exclusively on strings and `Buffer`
(or `Uint8Array`) objects. It is possible, however, for stream implementations
to work with other types of JavaScript values (with the exception of `null`,
which serves a special purpose within streams). Such streams are considered to
operate in "object mode".

Stream instances are switched into object mode using the `objectMode` option
when the stream is created. Attempting to switch an existing stream into object
mode ***is not safe***.[<sup>Object mode</sup>][object-mode]

Both `Writable` and `Readable` streams will store data in an internal buffer.

The amount of data potentially buffered depends on the `highWaterMark` option
passed into the stream's constructor. For normal streams, the `highWaterMark`
option specifies a total number of bytes. For streams operating in object mode,
the `highWaterMark` specifies a total number of objects.

> The use of `readable.setEncoding()` will change the behavior of how the
> `highWaterMark` operates in non-object mode.
>
> Typically, the size of the current buffer is measured against the `highWaterMark`
> in bytes. However, after `setEncoding()` is called, the comparison function
> will begin to measure the buffer's size ***in characters***.[<sup>highWaterMark</sup>][highwatermark]

Data is buffered in `Readable` streams when the implementation calls `stream.push(chunk)`.
If the consumer of the `Stream` does not call `stream.read()`, the data will sit
in the internal queue until it is consumed.

Once the total size of the internal read buffer reaches the threshold specified
by `highWaterMark`, the stream will temporarily stop reading data from the
underlying resource until the data currently buffered can be consumed (that is,
the stream will stop calling the internal `readable._read()` method that is used
to fill the read buffer).

Data is buffered in `Writable` streams when the `writable.write(chunk)` method
is called repeatedly. While the total size of the internal write buffer is below
the threshold set by `highWaterMark`, calls to `writable.write()` will return
true. Once the size of the internal buffer reaches or exceeds the `highWaterMark`,
false will be returned.

A key goal of the stream API, particularly the `stream.pipe()` method, is to
limit the buffering of data to acceptable levels such that sources and
destinations of differing speeds will not overwhelm the available memory.

The `highWaterMark` option is a threshold, not a limit: it dictates the amount
of data that a stream buffers before it stops asking for more data. It does not
enforce a strict memory limitation in general. Specific stream implementations
may choose to enforce stricter limits but doing so is optional.

Because `Duplex` and `Transform` streams are both `Readable` and `Writable`,
each maintains two separate internal buffers used for reading and writing,
allowing each side to operate independently of the other while maintaining an
appropriate and efficient flow of data.[<sup>Buffering</sup>][buffering]

## [Writable Streams][writable-streams]

Writable streams are an abstraction for a destination to which data is written.

Events of the writable streams:

- `close`
- `drain`
- `error`
- `finish`
- `pipe`
- `unpipe`

Key methods of the writable streams:

- `destroy([error])`
- `end([chunk[, encoding]][, callback])`
- `write(chunk[, encoding][, callback])`

Methods for custom implementation:[<sup>Implementing a writable stream</sup>][implementing-a-writable-stream]

- `_construct(callback)`
- `_write(chunk, encoding, callback)`
- `_writev(chunks, callback)`
- `_destroy(err, callback)`
- `_final(callback)`

### close

The `close` event is emitted when the stream and any of its underlying resources
(a file descriptor, for example) have been closed. The event indicates that no
more events will be emitted, and no further computation will occur.

A `Writable` stream will always emit the `close` event if it is created with the
`emitClose` option.

### drain

If a call to `stream.write(chunk)` returns false, the `drain` event will be
emitted when it is appropriate to resume writing data to the stream.

### error

The `error` event is emitted if an error occurred while writing or piping data.
The listener callback is passed a single `Error` argument when called.

The stream is closed when the `error` event is emitted unless the `autoDestroy`
option was set to false when creating the stream.

After `error`, no further events other than `close` should be emitted (including
`error` events).

### finish

The `finish` event is emitted after the `stream.end()` method has been called,
and all data has been flushed to the underlying system.

### `destroy([error])`

After this call, the writable stream has ended and subsequent calls to `write()`
or `end()` will result in an `ERR_STREAM_DESTROYED` error. This is a destructive
and immediate way to destroy a stream. Previous calls to `write()` may not have
drained, and may trigger an `ERR_STREAM_DESTROYED` error. Use `end()` instead of
destroy if data should flush before close, or wait for the `drain` event before
destroying the stream.

### `end([chunk[, encoding]][, callback])`

Calling the `writable.end()` method signals that no more data will be written to
the `Writable`. The optional chunk and encoding arguments allow one final
additional chunk of data to be written immediately before closing the stream.

### `write(chunk[, encoding][, callback])`

The return value is true if the internal buffer is less than the `highWaterMark`
configured when the stream was created after admitting chunk. If false is
returned, further attempts to write data to the stream should stop until the
`drain` event is emitted.

While a stream is not draining, calls to `write()` will buffer chunk, and return
false. Once all currently buffered chunks are drained (accepted for delivery by
the operating system), the `drain` event will be emitted. Once `write()` returns
false, do not write more chunks until the `drain` event is emitted. While
calling `write()` on a stream that is not draining is allowed, Node.js will
buffer all written chunks until maximum memory usage occurs, at which point it
will abort unconditionally.

### `_construct(callback)`

This optional function will be called in a tick after the stream constructor has
returned, delaying any `_write()`, `_final()` and `_destroy()` calls until
callback is called. This is useful to initialize state or asynchronously
initialize resources before the stream can be used.

### `_write(chunk, encoding, callback)`

The callback function must be called synchronously inside of `writable._write()`
or asynchronously (i.e. different tick) to signal either that the write
completed successfully or failed with an error. The first argument passed to the
callback must be the `Error` object if the call failed or null if the write
succeeded.

All calls to `writable.write()` that occur between the time `writable._write()`
is called and the callback is called will cause the written data to be buffered.
When the callback is invoked, the stream might emit a `drain` event. If a stream
implementation is capable of processing multiple chunks of data at once, the
`writable._writev()` method should be implemented.

If the `decodeStrings` property is explicitly set to false in the constructor
options, then chunk will remain the same object that is passed to `.write()`,
and may be a string rather than a `Buffer`. This is to support implementations
that have an optimized handling for certain string data encodings. In that case,
the encoding argument will indicate the character encoding of the string.
Otherwise, the encoding argument can be safely ignored.

### `_writev(chunks, callback)`

The `writable._writev()` method may be implemented in addition or alternatively
to `writable._write()` in stream implementations that are capable of processing
multiple chunks of data at once. If implemented and if there is buffered data
from previous writes, `_writev()` will be called instead of `_write()`.

### `_destroy(err, callback)`

The `_destroy()` method is called by `writable.destroy()`. The callback should
not be mixed with `async/await` once it is executed when a promise is resolved.

### `_final(callback)`

This optional function will be called before the stream closes, delaying the
`finish` event until callback is called. This is useful to close resources or
write buffered data before a stream ends.

> Errors occurring during the processing of the `writable._write()`, `writable._writev()`
> and `writable._final()` methods must be propagated by invoking the callback
> and passing the error as the first argument. Throwing an `Error` from within
> these methods or manually emitting an `error` event results in undefined
> behavior.

## [Readable Streams][readable-streams]

Readable streams are an abstraction for a source from which data is consumed.

Readable streams effectively operate in one of two modes: flowing and paused.

In flowing mode, data is read from the underlying system automatically and
provided to an application as quickly as possible using events via the
`EventEmitter` interface.

In paused mode, the `stream.read()` method must be called explicitly to read
chunks of data from the stream.

All `Readable` streams begin in paused mode but can be switched to flowing mode
in one of the following ways:

- Adding a `data` event handler.
- Calling the `stream.resume()` method.
- Calling the `stream.pipe()` method to send the data to a `Writable`.

The `Readable` can switch back to paused mode using one of the following:

- If there are no pipe destinations, by calling the `stream.pause()` method.
- If there are pipe destinations, by removing all pipe destinations. Multiple
  pipe destinations may be removed by calling the `stream.unpipe()` method.

The important concept to remember is that a `Readable` ***will not generate data***
until a mechanism for either consuming or ignoring that data is provided. If the
consuming mechanism is disabled or taken away, the Readable will ***attempt***
to stop generating the data.

For backward compatibility reasons, removing `data` event handlers ***will not automatically pause***
the stream. Also, if there are piped destinations, then calling `stream.pause()`
***will not guarantee*** that the stream will remain paused once those
destinations drain and ask for more data.

If a `Readable` is switched into flowing mode and there are no consumers
available to handle the data, that data will be lost. This can occur, for
instance, when the `readable.resume()` method is called without a listener
attached to the `data` event, or when a `data` event handler is removed from the
stream.

Adding a `readable` event handler automatically makes the stream stop flowing,
and the data has to be consumed via `readable.read()`. If the `readable` event
handler is removed, then the stream will start flowing again if there is a
`data` event handler.

The "two modes" of operation for a `Readable` stream are a simplified
abstraction for the more complicated internal state management that is happening
within the `Readable` stream implementation.

Specifically, at any given point in time, every `Readable` is in one of three
possible states:

- `readable.readableFlowing === null`
- `readable.readableFlowing === false`
- `readable.readableFlowing === true`

When `readable.readableFlowing` is `null`, no mechanism for consuming the
stream's data is provided. Therefore, the stream will not generate data. While
in this state, attaching a listener for the `data` event, calling the `readable.pipe()`
method, or calling the `readable.resume()` method will switch `readable.readableFlowing`
to true, causing the `Readable` to begin actively emitting events as data is
generated.

Calling `readable.pause()`, `readable.unpipe()`, or receiving backpressure[<sup>Backpressure</sup>][backpressure]
will cause the `readable.readableFlowing` to be set as false, temporarily
halting the flowing of events but not halting the generation of data. While in
this state, attaching a listener for the `data` event will not switch
`readable.readableFlowing` to true.

While `readable.readableFlowing` is false, data may be accumulating within the
stream's internal buffer.

> Using a combination of `on('data')`, `on('readable')`, `pipe()`, or async
> iterators could lead to unintuitive behavior.

Events of the readable streams:

- `close`
- `data`
- `end`
- `error`
- `pause`
- `readable`
- `resume`

Key methods of the readable streams:

- `destroy([error])`
- `pause()`
- `pipe(destination[, options])`
- `read([size])`
- `resume()`
- `unpipe([destination])`
- `push(chunk[, encoding])`

Methods for custom implementation:[<sup>Implementing a readable stream</sup>][implementing-a-readable-stream]

- `_construct(callback)`
- `_read(size)`
- `_destroy(err, callback)`

### close

The `close` event is emitted when the stream and any of its underlying resources
(a file descriptor, for example) have been closed. The event indicates that no
more events will be emitted, and no further computation will occur.

A `Readable` stream will always emit the `close` event if it is created with the
`emitClose` option.

### data

The `data` event is emitted whenever the stream is relinquishing ownership of a
chunk of data to a consumer. This may occur whenever the stream is switched in
flowing mode by calling `readable.pipe()`, `readable.resume()`, or by attaching
a listener callback to the `data` event. The `data` event will also be emitted
whenever the `readable.read()` method is called and a chunk of data is available
to be returned.

Attaching a `data` event listener to a stream that has not been explicitly
paused will switch the stream into flowing mode. Data will then be passed as
soon as it is available.

The listener callback will be passed the chunk of data as a string if a default
encoding has been specified for the stream using the `readable.setEncoding()`
method; otherwise the data will be passed as a `Buffer`.

### end

The `end` event is emitted when there is no more data to be consumed from the
stream.

The `end` event will not be emitted unless the data is completely consumed.

### error

The `error` event may be emitted by a `Readable` implementation at any time.

The listener callback will be passed a single `Error` object.

### readable

The `readable` event is emitted when there is data available to be read from the
stream or when the end of the stream has been reached. Effectively, the `readable`
event indicates that the stream has new information. If data is available,
`stream.read()` will return that data.

If the end of the stream has been reached, calling `stream.read()` will return
null and trigger the `end` event. This is also true if there never was any data
to be read.

In some cases, attaching a listener for the `readable` event will cause some
amount of data to be read into an internal buffer.

In general, the `readable.pipe()` and `data` event mechanisms are easier to
understand than the `readable` event. However, handling `readable` might result
in increased throughput.

If both `readable` and `data` are used at the same time, `readable` takes
precedence in controlling the flow, i.e. `data` will be emitted only when
`stream.read()` is called. The `readableFlowing` property would become false. If
there are `data` listeners when `readable` is removed, the stream will start
flowing, i.e. `data` events will be emitted without calling `.resume()`.

### `destroy([error])`

After this call, the readable stream will release any internal resources and
subsequent calls to `push()` will be ignored.

Once `destroy()` has been called any further calls will be a no-op and no
further errors except from `_destroy()` may be emitted as `error`.

### `pause()`

The `readable.pause()` method will cause a stream in flowing mode to stop
emitting `data` events, switching out of flowing mode. Any data that becomes
available will remain in the internal buffer.

The `readable.pause()` method has no effect if there is a `readable` event
listener.

### `pipe(destination[, options])`

The `readable.pipe()` method attaches a `Writable` stream to the readable,
causing it to switch automatically into flowing mode and push all of its data to
the attached `Writable`. The flow of data will be automatically managed so that
the destination `Writable` stream is not overwhelmed by a faster `Readable`
stream.

It is possible to attach multiple `Writable` streams to a single `Readable`
stream.

The `readable.pipe()` method returns a reference to the destination stream
making it possible to set up chains of piped streams.

By default, `stream.end()` is called on the destination `Writable` stream when
the source `Readable` stream emits `end`, so that the destination is no longer
writable. To disable this default behavior, the `end` option can be passed as
false, causing the destination stream to remain open.

One important caveat is that if the `Readable` stream emits an error during
processing, the `Writable` destination is not closed automatically. If an error
occurs, it will be necessary to manually close each stream in order to prevent
memory leaks.

### `read([size])`

The `readable.read()` method reads data out of the internal buffer and returns
it. If no data is available to be read, `null` is returned. By default, the data
is returned as a `Buffer` object unless an encoding has been specified using the
`readable.setEncoding()` method or the stream is operating in object mode.

The optional size argument specifies a specific number of bytes to read. If size
bytes are not available to be read, null will be returned unless the stream has
ended, in which case all of the data remaining in the internal buffer will be
returned.

If the size argument is not specified, all of the data contained in the internal
buffer will be returned.

The size argument must be less than or equal to 1 GiB.

The `readable.read()` method should only be called on `Readable` streams
operating in paused mode. In flowing mode, `readable.read()` is called
automatically until the internal buffer is fully drained.

When reading a large file `.read()` may return `null`, having consumed all
buffered content so far, but there is still more data to come not yet buffered.
In this case a new `readable` event will be emitted when there is more data in
the buffer. Finally the `end` event will be emitted when there is no more data
to come.

A `Readable` stream in object mode will always return a single item from a call
to `readable.read(size)`, regardless of the value of the size argument.

If the `readable.read()` method returns a chunk of data, a `data` event will
also be emitted.

### `resume()`

The `readable.resume()` method causes an explicitly paused `Readable` stream to
resume emitting `data` events, switching the stream into flowing mode.

The `readable.resume()` method can be used to fully consume the data from a
stream without actually processing any of that data.

The `readable.resume()` method has no effect if there is a `readable` event
listener.

### `unpipe([destination])`

The `readable.unpipe()` method detaches a `Writable` stream previously attached
using the `stream.pipe()` method.

If the destination is not specified, then all pipes are detached.

### `_construct(callback)`

This optional function will be scheduled in the next tick by the stream
constructor, delaying any `_read()` and `_destroy()` calls until callback is
called. This is useful to initialize state or asynchronously initialize
resources before the stream can be used.

### `_read(size)`

All `Readable` stream implementations must provide an implementation of the
`readable._read()` method to fetch data from the underlying resource.

When `readable._read()` is called, if data is available from the resource, the
implementation should begin pushing that data into the read queue using the
`this.push(dataChunk)` method. `_read()` will be called again after each call to
`this.push(dataChunk)` once the stream is ready to accept more data. `_read()`
may continue reading from the resource and pushing data until `readable.push()`
returns false. Only when `_read()` is called again after it has stopped, it
should resume pushing additional data into the queue.

Once the `readable._read()` method has been called, it will not be called again
until more data is pushed through the `readable.push()` method. Empty data such
as empty buffers and strings will not cause `readable._read()` to be called.

The size argument is advisory. For implementations where a "read" is a single
operation that returns data can use the size argument to determine how much data
to fetch. Other implementations may ignore this argument and simply provide data
whenever it becomes available. There is no need to "wait" until size bytes are
available before calling `stream.push(chunk)`.

> Errors occurring during processing of the `readable._read()` must be
> propagated through the `readable.destroy(err)` method. Throwing an `Error`
> from within `readable._read()` or manually emitting an `error` event results
> in undefined behavior.

### `_destroy(err, callback)`

The `_destroy()` method is called by `readable.destroy()`. It can be overridden
by child classes but it must not be called directly.

### `push(chunk[, encoding])`

When chunk is a `Buffer`, `Uint8Array`, or `string`, the chunk of data will be
added to the internal queue for users of the stream to consume. Passing chunk as
`null` signals the end of the stream (EOF), after which no more data can be
written.

When the `Readable` is operating in paused mode, the data added with `readable.push()`
can be read out by calling the `readable.read()` method when the `readable`
event is emitted.

When the `Readable` is operating in flowing mode, the data added with `readable.push()`
will be delivered by emitting a `data` event.

The `readable.push()` method is used to push the content into the internal
buffer. It can be driven by the `readable._read()` method.

For streams not operating in object mode, if the chunk parameter of `readable.push()`
is undefined, it will be treated as empty string or buffer.

> Use of `readable.push('')` is not recommended.
>
> Pushing a zero-byte string, `Buffer`, or `Uint8Array` to a stream that is not
> in object mode has an interesting side effect. Because it is a call to `readable.push()`,
> the call will end the reading process. However, because the argument is an
> empty string, no data is added to the readable buffer so there is nothing for a
> user to consume.[<sup>readable.push('')</sup>][readablepush]

[//]: # "Links"

[types-of-streams]: https://nodejs.org/dist/latest-v18.x/docs/api/stream.html#types-of-streams
[object-mode]: https://nodejs.org/dist/latest-v18.x/docs/api/stream.html#object-mode
[highwatermark]: https://nodejs.org/dist/latest-v18.x/docs/api/stream.html#highwatermark-discrepancy-after-calling-readablesetencoding
[buffering]: https://nodejs.org/dist/latest-v18.x/docs/api/stream.html#buffering
[writable-streams]: https://nodejs.org/dist/latest-v18.x/docs/api/stream.html#writable-streams
[implementing-a-writable-stream]: https://nodejs.org/dist/latest-v18.x/docs/api/stream.html#implementing-a-writable-stream
[readable-streams]: https://nodejs.org/dist/latest-v18.x/docs/api/stream.html#readable-streams
[backpressure]: https://nodejs.org/en/docs/guides/backpressuring-in-streams#how-does-backpressure-resolve-these-issues
[implementing-a-readable-stream]: https://nodejs.org/dist/latest-v18.x/docs/api/stream.html#implementing-a-readable-stream
[readablepush]: https://nodejs.org/dist/latest-v18.x/docs/api/stream.html#readablepush
