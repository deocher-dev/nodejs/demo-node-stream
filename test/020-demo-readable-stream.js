import assert from 'node:assert/strict'

import {
  describe,
  it
} from 'node:test'

import DemoReadable from './util/DemoReadable.js'
import DemoDataSourceAsync from './util/DemoDataSource/DemoDataSourceAsync.js'

const { AssertionError } = assert

describe('#020 Test stream API', () => {
  describe('#020.1 of DemoReadable', () => {
    it('#.1 using `readable` event', (ctx, done) => {
      const dataSource = new DemoDataSourceAsync(['1', '2', '3'])
      const readable = new DemoReadable(dataSource, { encoding: 'utf8' })

      const chunks = []

      readable.on('error', done)

      readable.on('readable', () => {
        let chunk = readable.read()

        while (chunk !== null) {
          chunks.push(chunk)

          chunk = readable.read()
        }
      })

      readable.on('end', () => {
        try {
          assert.deepEqual(chunks, ['1', '2', '3'], 'Invalid collection of chunks.')

          done()
        } catch (err) {
          done(err)
        }
      })
    })

    it('#.2 using `readable` event with error handling', (ctx, done) => {
      const dataSource = new DemoDataSourceAsync(['1', '22', '3'], { maxChunkLength: 1 })
      const readable = new DemoReadable(dataSource, { encoding: 'utf8' })

      readable.on('error', err => {
        try {
          assert.equal(err.message, 'Invalid chunk.', 'Inappropriate error.')

          done()
        } catch (err) {
          done(err)
        }
      })

      readable.on('readable', () => {
        while (readable.read() !== null);
      })

      readable.on('end', () => {
        done(new AssertionError({ message: 'No errors were raised.' }))
      })
    })

    it('#.3 using `data` event', (ctx, done) => {
      const dataSource = new DemoDataSourceAsync(['1', '2', '3'])
      const readable = new DemoReadable(dataSource, { encoding: 'utf8' })

      const chunks = []

      readable.on('error', done)

      readable.on('data', chunk => {
        chunks.push(chunk)
      })

      readable.on('end', () => {
        try {
          assert.deepEqual(chunks, ['1', '2', '3'], 'Invalid collection of chunks.')

          done()
        } catch (err) {
          done(err)
        }
      })
    })

    it('#.4 using `data` event with error handling', (ctx, done) => {
      const dataSource = new DemoDataSourceAsync(['1', '22', '3'], { maxChunkLength: 1 })
      const readable = new DemoReadable(dataSource, { encoding: 'utf8' })

      readable.on('error', err => {
        try {
          assert.equal(err.message, 'Invalid chunk.', 'Inappropriate error.')

          done()
        } catch (err) {
          done(err)
        }
      })

      readable.on('data', chunk => {
      })

      readable.on('end', () => {
        done(new AssertionError({ message: 'No errors were raised.' }))
      })
    })
  })
})
