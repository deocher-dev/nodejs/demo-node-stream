import assert from 'node:assert/strict'

import {
  describe,
  it
} from 'node:test'

import DemoReadable from './util/DemoReadable.js'
import DemoWritable from './util/DemoWritable.js'
import DemoDataSourceAsync from './util/DemoDataSource/DemoDataSourceAsync.js'
import DemoDataStoreAsync from './util/DemoDataStore/DemoDataStoreAsync.js'

const { AssertionError } = assert

describe('#030 Test stream API', () => {
  it('#.1 using `pipe()` method', (ctx, done) => {
    const dataSource = new DemoDataSourceAsync(['1', '2', '3'])
    const dataStore = new DemoDataStoreAsync()

    const readable = new DemoReadable(dataSource, { encoding: 'utf8' })
    const writable = new DemoWritable(dataStore)

    writable.on('pipe', () => {})
    writable.on('unpipe', () => {})

    readable.on('error', err => {
      if (!writable.destroyed) {
        writable.destroy()
      }

      done(err)
    })

    writable.on('error', err => {
      if (!readable.destroyed) {
        readable.destroy()
      }

      done(err)
    })

    writable.on('finish', () => {
      try {
        assert.deepEqual(dataStore.getChunks(), ['1', '2', '3'], 'Invalid collection of chunks.')

        done()
      } catch (err) {
        done(err)
      }
    })

    readable.pipe(writable)
  })

  it('#.2 using `pipe()` method with readable error', (ctx, done) => {
    const dataSource = new DemoDataSourceAsync(['1', '22', '3'], { maxChunkLength: 1 })
    const dataStore = new DemoDataStoreAsync()

    const readable = new DemoReadable(dataSource, { encoding: 'utf8' })
    const writable = new DemoWritable(dataStore)

    writable.on('pipe', () => {})
    writable.on('unpipe', () => {})

    readable.on('error', err => {
      try {
        if (!writable.destroyed) {
          writable.destroy()
        }

        assert.equal(err.message, 'Invalid chunk.', 'Inappropriate error.')

        done()
      } catch (err) {
        done(err)
      }
    })

    writable.on('error', err => {
      if (!readable.destroyed) {
        readable.destroy()
      }

      done(err)
    })

    writable.on('finish', () => {
      done(new AssertionError({ message: 'No errors were raised.' }))
    })

    readable.pipe(writable)
  })

  it('#.3 using `pipe()` method with writable error', (ctx, done) => {
    const dataSource = new DemoDataSourceAsync(['1', '22', '3'])
    const dataStore = new DemoDataStoreAsync({ maxChunkLength: 1 })

    const readable = new DemoReadable(dataSource, { encoding: 'utf8' })
    const writable = new DemoWritable(dataStore)

    writable.on('pipe', () => {})
    writable.on('unpipe', () => {})

    readable.on('error', err => {
      if (!writable.destroyed) {
        writable.destroy()
      }

      done(err)
    })

    writable.on('error', err => {
      try {
        if (!readable.destroyed) {
          readable.destroy()
        }

        assert.equal(err.message, 'Invalid chunk.', 'Inappropriate error.')

        done()
      } catch (err) {
        done(err)
      }
    })

    writable.on('finish', () => {
      done(new AssertionError({ message: 'No errors were raised.' }))
    })

    readable.pipe(writable)
  })
})
