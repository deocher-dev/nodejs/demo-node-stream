import assert from 'node:assert/strict'

import {
  describe,
  it
} from 'node:test'

import DemoWritable from './util/DemoWritable.js'
import DemoDataStoreAsync from './util/DemoDataStore/DemoDataStoreAsync.js'

const { AssertionError } = assert

describe('#010 Test stream API', () => {
  describe('#010.1 of DemoWritable', () => {
    it('#.1 using `write()/end()`', (ctx, done) => {
      const dataStore = new DemoDataStoreAsync()
      const writable = new DemoWritable(dataStore)

      writable.on('error', done)

      writable.on('finish', () => {
        try {
          assert.deepEqual(dataStore.getChunks(), ['1', '2', '3'], 'Invalid collection of chunks.')

          done()
        } catch (err) {
          done(err)
        }
      })

      Promise.resolve()
        .then(() => writable.write('1'))
        .then(() => writable.writableNeedDrain && awaitForDrain)
        .then(() => writable.write('2'))
        .then(() => writable.writableNeedDrain && awaitForDrain)
        .then(() => writable.end('3'))

      function awaitForDrain () {
        return new Promise(resolve => writable.once('drain', resolve))
      }
    })

    it('#.2 using `write()/end()` with error handling', (ctx, done) => {
      const dataStore = new DemoDataStoreAsync({ maxChunkLength: 1 })
      const writable = new DemoWritable(dataStore)

      writable.on('error', err => {
        try {
          assert.equal(err.message, 'Invalid chunk.', 'Inappropriate error.')

          done()
        } catch (err) {
          done(err)
        }
      })

      writable.on('finish', () => {
        done(new AssertionError({ message: 'No errors were raised.' }))
      })

      Promise.resolve()
        .then(() => writable.write('1'))
        .then(() => writable.writableNeedDrain && awaitForDrain)
        .then(() => writable.write('22'))
        .then(() => writable.writableNeedDrain && awaitForDrain)
        .then(() => writable.end('3'))

      function awaitForDrain () {
        return new Promise(resolve => writable.once('drain', resolve))
      }
    })
  })
})
