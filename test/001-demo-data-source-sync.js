import assert from 'node:assert/strict'

import {
  describe,
  it
} from 'node:test'

import {
  assertIterableIterator,
  assertIteratorResult
} from './util/assert-iteration-protocols.js'

import {
  DemoDataSourceSync
} from './util/DemoDataSource/DemoDataSourceSync.js'

describe('#001 Sync data source', () => {
  describe('#001.1 interface', () => {
    const dataSource = new DemoDataSourceSync(['1'])

    it('#.1 implements iterable iterator', ctx => {
      assertIterableIterator(dataSource)
    })
  })

  describe('#001.2 `next()` method', () => {
    const dataSource = new DemoDataSourceSync(['1', '2', '3'])

    it('#.1 returns iterator result for the first item', ctx => {
      assertIteratorResult(dataSource.next(), { done: false, value: '1' })
    })

    it('#.2 returns iterator result for the second item', ctx => {
      assertIteratorResult(dataSource.next(), { done: false, value: '2' })
    })

    it('#.3 returns iterator result for the last item', ctx => {
      assertIteratorResult(dataSource.next(), { done: false, value: '3' })
    })

    it('#.4 returns iterator result after the last item', ctx => {
      assertIteratorResult(dataSource.next(), { done: true, value: undefined })
    })
  })

  describe('#001.3 `return()` method', () => {
    const dataSource = new DemoDataSourceSync(['1'])

    it('#.1 does not consume the next item', ctx => {
      assert.equal(dataSource.getConsumed(), 0, 'Data source item was consumed.')

      assertIteratorResult(dataSource.return(), { done: true, value: '1' })

      assert.equal(dataSource.getConsumed(), 0, 'Data source item was consumed.')

      assertIteratorResult(dataSource.next(), { done: false, value: '1' })

      assert.equal(dataSource.getConsumed(), 1, 'Data source item was not consumed.')
    })
  })

  describe('#001.4 `throw()` method', () => {
    const dataSource = new DemoDataSourceSync(['1'])

    it('#.1 does not consume the next item', ctx => {
      assert.equal(dataSource.getConsumed(), 0, 'Data source item was consumed.')

      assertIteratorResult(dataSource.throw(), { done: true, value: '1' })

      assert.equal(dataSource.getConsumed(), 0, 'Data source item was consumed.')

      assertIteratorResult(dataSource.next(), { done: false, value: '1' })

      assert.equal(dataSource.getConsumed(), 1, 'Data source item was not consumed.')
    })
  })

  describe('#001.5 produces', () => {
    it('#.1 0 items when capacity is 0', ctx => {
      assertIterations(new DemoDataSourceSync([]))
    })

    it('#.2 1 item when capacity is 1', ctx => {
      assertIterations(new DemoDataSourceSync(['1']))
    })

    it('#.3 2 items when capacity is 2', ctx => {
      assertIterations(new DemoDataSourceSync(['1', '2']))
    })

    function assertIterations (dataSource) {
      let counter = 0

      for (const value of dataSource) {
        counter++

        assert.equal(value, String(counter), 'Incorrect data source value.')
      }

      assert.equal(counter, dataSource.getCapacity(), 'Incorrect amount of data source items.')
    }
  })
})
