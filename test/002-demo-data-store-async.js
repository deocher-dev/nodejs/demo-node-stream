import assert from 'node:assert/strict'

import {
  describe,
  it
} from 'node:test'

import {
  DemoDataStoreAsync
} from './util/DemoDataStore/DemoDataStoreAsync.js'

const DEFAULT_ENCODING = 'utf8'

describe('#002 Async data store', () => {
  describe('#002.1 when created', () => {
    it('#.1 is empty and not finished', async ctx => {
      const dataStore = new DemoDataStoreAsync()

      assert.equal(dataStore.getChunks().length, 0, 'Is not empty.')
      assert.equal(dataStore.isFinished(), false, 'Is considered finished.')
    })
  })

  describe('#002.2 when is empty', () => {
    it('#.1 becomes finished when `finish()` is called', async ctx => {
      const dataStore = new DemoDataStoreAsync()

      dataStore.finish()

      assert.equal(dataStore.isFinished(), true, 'Is not finished.')
    })

    it('#.2 remains empty and not finished when `init()` is called', async ctx => {
      const dataStore = new DemoDataStoreAsync()

      dataStore.finish()
      dataStore.init()

      assert.equal(dataStore.getChunks().length, 0, 'Is not empty.')
      assert.equal(dataStore.isFinished(), false, 'Is considered finished.')
    })

    it('#.3 remains empty and not finished when `reset()` is called', async ctx => {
      const dataStore = new DemoDataStoreAsync()

      dataStore.finish()
      dataStore.reset()

      assert.equal(dataStore.getChunks().length, 0, 'Is not empty.')
      assert.equal(dataStore.isFinished(), false, 'Is considered finished.')
    })

    it('#.4 accepts one chunk when `push()` is called', async ctx => {
      const dataStore = new DemoDataStoreAsync()

      await dataStore.push({ chunk: '1', encoding: DEFAULT_ENCODING })

      assert.deepEqual(dataStore.getChunks(), ['1'], 'Invalid collection of chunks.')
    })

    it('#.5 accepts several chunks when `push()` is called', async ctx => {
      const dataStore = new DemoDataStoreAsync()

      await dataStore.push(
        { chunk: '1', encoding: DEFAULT_ENCODING },
        { chunk: '2', encoding: DEFAULT_ENCODING }
      )

      assert.deepEqual(dataStore.getChunks(), ['1', '2'], 'Invalid collection of chunks.')
    })
  })

  describe('#002.3 when is not empty', () => {
    it('#.1 becomes finished when `finish()` is called', async ctx => {
      const dataStore = new DemoDataStoreAsync()

      await dataStore.push({ chunk: '1', encoding: DEFAULT_ENCODING })
      dataStore.finish()

      assert.equal(dataStore.isFinished(), true, 'Is not finished.')
    })

    it('#.2 becomes empty and not finished when `init()` is called', async ctx => {
      const dataStore = new DemoDataStoreAsync()

      await dataStore.push({ chunk: '1', encoding: DEFAULT_ENCODING })
      dataStore.finish()
      dataStore.init()

      assert.equal(dataStore.getChunks().length, 0, 'Is not empty.')
      assert.equal(dataStore.isFinished(), false, 'Is considered finished.')
    })

    it('#.3 becomes empty and not finished when `reset()` is called', async ctx => {
      const dataStore = new DemoDataStoreAsync()

      await dataStore.push({ chunk: '1', encoding: DEFAULT_ENCODING })
      dataStore.finish()
      dataStore.reset()

      assert.equal(dataStore.getChunks().length, 0, 'Is not empty.')
      assert.equal(dataStore.isFinished(), false, 'Is considered finished.')
    })

    it('#.4 accepts one chunk when `push()` is called', async ctx => {
      const dataStore = new DemoDataStoreAsync()

      await dataStore.push({ chunk: '1', encoding: DEFAULT_ENCODING })
      await dataStore.push({ chunk: '1', encoding: DEFAULT_ENCODING })

      assert.deepEqual(dataStore.getChunks(), ['1', '1'], 'Invalid collection of chunks.')
    })

    it('#.5 accepts several chunks when `push()` is called', async ctx => {
      const dataStore = new DemoDataStoreAsync()

      await dataStore.push({ chunk: '1', encoding: DEFAULT_ENCODING })
      await dataStore.push(
        { chunk: '1', encoding: DEFAULT_ENCODING },
        { chunk: '2', encoding: DEFAULT_ENCODING }
      )

      assert.deepEqual(dataStore.getChunks(), ['1', '1', '2'], 'Invalid collection of chunks.')
    })
  })

  describe('#002.4 when buffer encoding is allowed', () => {
    it('#.1 encodes buffers as strings', async ctx => {
      const dataStore = new DemoDataStoreAsync({
        encodeBuffers: true
      })

      const buffer01 = Buffer.from('1', DEFAULT_ENCODING)
      const buffer02 = Buffer.from('1', DEFAULT_ENCODING)
      const buffer03 = Buffer.from('2', DEFAULT_ENCODING)

      await dataStore.push({ chunk: buffer01, encoding: DEFAULT_ENCODING })
      await dataStore.push(
        { chunk: buffer02, encoding: DEFAULT_ENCODING },
        { chunk: buffer03, encoding: DEFAULT_ENCODING }
      )

      assert.deepEqual(dataStore.getChunks(), ['1', '1', '2'], 'Invalid collection of chunks.')
    })
  })

  describe('#002.5 when buffer encoding is not allowed', () => {
    it('#.1 does not encode buffers', async ctx => {
      const dataStore = new DemoDataStoreAsync({
        encodeBuffers: false
      })

      const buffer01 = Buffer.from('1', DEFAULT_ENCODING)
      const buffer02 = Buffer.from('1', DEFAULT_ENCODING)
      const buffer03 = Buffer.from('2', DEFAULT_ENCODING)

      await dataStore.push({ chunk: buffer01, encoding: DEFAULT_ENCODING })
      await dataStore.push(
        { chunk: buffer02, encoding: DEFAULT_ENCODING },
        { chunk: buffer03, encoding: DEFAULT_ENCODING }
      )

      const chunks = dataStore.getChunks()

      assert.equal(Buffer.compare(chunks[0], buffer01), 0, 'Invalid chunk.')
      assert.equal(Buffer.compare(chunks[1], buffer02), 0, 'Invalid chunk.')
      assert.equal(Buffer.compare(chunks[2], buffer03), 0, 'Invalid chunk.')
    })
  })
})
