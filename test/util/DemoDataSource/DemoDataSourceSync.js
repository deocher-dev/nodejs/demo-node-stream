import DemoDataSourceBase from './DemoDataSourceBase.js'

/**
 * The demo data source. Implements synchronous iterable iterator.
 *
 * @extends module:test-util/DemoDataSource/DemoDataSourceBase.DemoDataSourceBase
 *
 * @example
 * const dataSource = new DemoDataSourceSync(chunks)
 *
 * console.debug(dataSource === dataSource[Symbol.iterator]()) // true
 */
export class DemoDataSourceSync extends DemoDataSourceBase {
  /**
   * @returns {DemoDataSourceSync} A reference to the current instance.
   */
  [Symbol.iterator] () {
    return this
  }

  /**
   * The method consumes the next item of the data source.
   *
   * @param {any} [value]
   *   If provided, the value will be returned instead of the data source item.
   * @returns {IteratorResult}
   */
  next (value) {
    const item = {
      done: true,
      value: undefined
    }

    if (!this.isFinished()) {
      item.done = false
      item.value = this._getNextValue(value, this._nextIndex)

      this._nextIndex++
    }

    return item
  }

  /**
   * The method returns the next item of the data source. The subsequent calls
   * will return the same item again and again.
   *
   * The method does not provide any cleanup and does not consume an item of the
   * data source. A subsequent call of the `next()` method will consume and
   * return the next item as usual.
   *
   * @param {any} [value]
   * @returns {IteratorResult}
   */
  return (value) {
    const item = {
      done: true,
      value: this._getReturnValue(value, this._nextIndex)
    }

    return item
  }

  /**
   * The method returns the next item of the data source. The subsequent calls
   * will return the same item again and again.
   *
   * The method does not provide any cleanup and does not consume an item of the
   * data source. A subsequent call of the `next()` method will return the next
   * item as usual.
   *
   * @param {any} [err]
   *   An exception detected while iterating through the data source.
   * @returns {IteratorResult}
   */
  throw (err) {
    const item = {
      done: true,
      value: this._getThrowValue(err, this._nextIndex)
    }

    return item
  }

  _getNextValue (value, nextIndex) {
    const chunk = this._chunks[nextIndex]

    if (!this.isValidChunk(chunk)) {
      throw new Error('Invalid chunk.')
    }

    return chunk
  }

  _getReturnValue (value, nextIndex) {
    return this._chunks[nextIndex]
  }

  _getThrowValue (err, nextIndex) { // eslint-disable-line n/handle-callback-err
    return this._chunks[nextIndex]
  }
}

export default DemoDataSourceSync
