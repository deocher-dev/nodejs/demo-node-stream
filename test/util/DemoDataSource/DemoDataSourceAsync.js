import { setTimeout } from 'timers/promises'

import DemoDataSourceBase from './DemoDataSourceBase.js'

import '../typedefs.js'

const DEFAULT_DELAY_MS = 100

/**
 * The demo data source. Implements asynchronous iterable iterator.
 *
 * @extends module:test-util/DemoDataSource/DemoDataSourceBase.DemoDataSourceBase
 *
 * @example
 * const dataSource = new DemoDataSourceAsync(chunks)
 *
 * console.debug(dataSource === dataSource[Symbol.asyncIterator]()) // true
 */
export class DemoDataSourceAsync extends DemoDataSourceBase {
  _delay = DEFAULT_DELAY_MS

  /**
   * @param {Chunk[]} chunks
   *   Chunks of data. Each call of the `next()` method returns the next
   *   unconsumed chunk (either a string or a Buffer).
   * @param {Object} [options]
   * @param {number} [options.delay=200]
   *   The delay for async operations in milliseconds.
   * @param {number} [options.maxChunkLength=1024]
   *   The maximum length constraint for a chunk. The chunk that exceeds the
   *   provided constraint is considered as invalid. It is possible to provide
   *   an invalid chunk in the `chunks` argument, while the `next()` call will
   *   be rejected with a `new Error('Invalid chunk.')` error.
   */
  constructor (chunks, options = {}) {
    const { delay = DEFAULT_DELAY_MS } = options

    super(chunks, options)

    this._delay = delay
  }

  /**
   * @returns {DemoDataSourceAsync} A reference to the current instance.
   */
  [Symbol.asyncIterator] () {
    return this
  }

  /**
   * The method consumes the next item of the data source.
   *
   * @param {any} [value]
   *   If provided, the value will be returned instead of the data source item.
   * @returns {Promise.<IteratorResult>}
   */
  async next (value) {
    const item = {
      done: true,
      value: undefined
    }

    if (!this.isFinished()) {
      item.done = false
      item.value = await this._getNextValue(value, this._nextIndex)

      this._nextIndex++
    }

    return item
  }

  /**
   * The method returns the next item of the data source. The subsequent calls
   * will return the same item again and again.
   *
   * The method does not provide any cleanup and does not consume an item of the
   * data source. A subsequent call of the `next()` method will consume and
   * return the next item as usual.
   *
   * @param {any} [value]
   * @returns {Promise.<IteratorResult>}
   */
  async return (value) {
    const item = {
      done: true,
      value: await this._getReturnValue(value, this._nextIndex)
    }

    return item
  }

  /**
   * The method returns the next item of the data source. The subsequent calls
   * will return the same item again and again.
   *
   * The method does not provide any cleanup and does not consume an item of the
   * data source. A subsequent call of the `next()` method will return the next
   * item as usual.
   *
   * @param {any} [err]
   *   An exception detected while iterating through the data source.
   * @returns {Promise.<IteratorResult>}
   */
  async throw (err) {
    const item = {
      done: true,
      value: await this._getThrowValue(err, this._nextIndex)
    }

    return item
  }

  /**
   * Get the delay for async operations.
   *
   * @returns {number}
   */
  getDelay () {
    return this._delay
  }

  async _getNextValue (value, nextIndex) {
    await setTimeout(this._delay)

    const chunk = this._chunks[nextIndex]

    if (!this.isValidChunk(chunk)) {
      throw new Error('Invalid chunk.')
    }

    return chunk
  }

  async _getReturnValue (value, nextIndex) {
    return await setTimeout(this._delay, this._chunks[nextIndex])
  }

  async _getThrowValue (err, nextIndex) { // eslint-disable-line n/handle-callback-err
    return await setTimeout(this._delay, this._chunks[nextIndex])
  }
}

export default DemoDataSourceAsync
