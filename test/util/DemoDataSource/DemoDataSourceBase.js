import '../typedefs.js'

const DEFAULT_MAX_CHUNK_LENGTH = 1024

/**
 * The base class for demo data sources. Descendants of this class are supposed
 * to be used as a source of data for readable streams.
 *
 * The class implements:
 * - symbols: `[Symbol.for('DemoReadable._construct')]()` and `[Symbol.for('DemoReadable._destroy')]()`.
 *   These methods should be called within `_construct()` and `_destroy()`
 *   methods of the `Readable` stream respectively.
 * - iterator methods: `next()`, `return()`, `throw()`.
 *   These abstract methods define the iterator protocol and should be overriden
 *   by descendants.
 *
 * @see {@link module:test-util/DemoReadable DemoReadable}
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Iteration_protocols#the_iterator_protocol Iterator Protocol}
 */
export class DemoDataSourceBase {
  _chunks = []
  _nextIndex = 0

  _maxChunkLength = DEFAULT_MAX_CHUNK_LENGTH

  /**
   * @param {Chunk[]} chunks
   *   Chunks of data. Each call of the `next()` method should return the next
   *   unconsumed chunk (either a string or a Buffer).
   * @param {Object} [options]
   * @param {number} [options.maxChunkLength=1024]
   *   The maximum length constraint for a chunk. The chunk that exceeds the
   *   provided constraint is considered as invalid. However, it is possible to
   *   provide an invalid chunk in the `chunks` argument, no error will be
   *   thrown.
   */
  constructor (chunks, options = {}) {
    const { maxChunkLength = DEFAULT_MAX_CHUNK_LENGTH } = options

    if (!Number.isInteger(maxChunkLength)) {
      throw new Error('Invalid `maxChunkLength` option.')
    }
    if (maxChunkLength < 0) {
      throw new Error('Invalid `maxChunkLength` option.')
    }

    this._maxChunkLength = maxChunkLength

    this.init(chunks)
  }

  [Symbol.for('DemoReadable._construct')] () {
    this.init()
  }

  [Symbol.for('DemoReadable._destroy')] () {
    this.reset()
  }

  /**
   * Init/reinit the state of the data source.
   *
   * @param {Chunk[]} [chunks] New chunks of data.
   */
  init (chunks) {
    if (chunks != null) {
      if (!Array.isArray(chunks)) {
        throw new Error('Invalid chunks provided.')
      }

      this._chunks = structuredClone(chunks)
    }

    this.reset()
  }

  /**
   * Reset the state of the data source.
   */
  reset () {
    this._nextIndex = 0
  }

  /**
   * This method is abstract. Should be defined by descendants.
   *
   * @throws {Error} 'Abstract method not implemented.'
   *
   * @abstract
   */
  next () {
    throw new Error('Abstract method not implemented.')
  }

  /**
   * This method is abstract. Should be defined by descendants.
   *
   * @throws {Error} 'Abstract method not implemented.'
   *
   * @abstract
   */
  return () {
    throw new Error('Abstract method not implemented.')
  }

  /**
   * This method is abstract. Should be defined by descendants.
   *
   * @throws {Error} 'Abstract method not implemented.'
   *
   * @abstract
   */
  throw () {
    throw new Error('Abstract method not implemented.')
  }

  /**
   * Get the total number of chunks.
   *
   * @returns {number}
   */
  getCapacity () {
    return this._chunks.length
  }

  /**
   * Get the number of consumed chunks.
   *
   * @returns {number}
   */
  getConsumed () {
    return this._nextIndex
  }

  /**
   * Get the value of `options.maxChunkLength` provided to the constructor.
   *
   * @returns {number}
   */
  getMaxChunkLength () {
    return this._maxChunkLength
  }

  /**
   * Check if the data source is fully consumed.
   *
   * @returns {boolean}
   */
  isFinished () {
    return this.getConsumed() === this.getCapacity()
  }

  /**
   * Check if the chunk is valid.
   *
   * @param {Chunk} chunk
   * @returns {boolean}
   */
  isValidChunk (chunk) {
    if (typeof chunk === 'string' || Buffer.isBuffer(chunk)) {
      return chunk.length < this._maxChunkLength
    }

    return true
  }
}

export default DemoDataSourceBase
