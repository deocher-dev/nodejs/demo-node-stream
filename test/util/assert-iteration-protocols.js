/**
 * This module contains assertion functions for the iteration protocols.
 *
 * @module
 *
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Iteration_protocols Iteration Protocols}
 */

import assert from 'node:assert/strict'

import { assertObject } from './assert-common.js'

/**
 * Checks if the object implements the iterable protocol.
 *
 * @throws {AssertionError}
 * @param {Object} obj
 */
export function assertIterableSync (obj) {
  assert.equal(typeof obj[Symbol.iterator], 'function', 'Is not iterable: has no `[Symbol.iterator]()` method.')
}

/**
 * Checks if the object implements the iterator protocol.
 *
 * @throws {AssertionError}
 * @param {Object} obj
 */
export function assertIteratorSync (obj) {
  assert.equal(typeof obj.next, 'function', 'Is not iterator: has no `next()` method.')

  if ('return' in obj) {
    assert.equal(typeof obj.return, 'function', 'Is not iterator: has no `return()` method.')
  }
  if ('throw' in obj) {
    assert.equal(typeof obj.throw, 'function', 'Is not iterator: has no `throw()` method.')
  }
}

/**
 * Checks if the object implements the iterable iterator protocol.
 *
 * @throws {AssertionError}
 * @param {Object} obj
 */
export function assertIterableIteratorSync (obj) {
  assertIterableSync(obj)
  assertIteratorSync(obj)

  const iterable = obj
  const iterator = obj[Symbol.iterator]()

  assert.equal(iterable, iterator, 'Is not iterable iterator: `iterable !== iterable[Symbol.iterator]()`.')
}

/**
 * Checks if the object is a valid iterator result.
 *
 * @throws {AssertionError}
 * @param {Object} actualResult
 * @param {Object} [expectedResult]
 */
export function assertIteratorResultSync (actualResult, expectedResult) {
  assertObject(actualResult)

  if ('done' in actualResult) {
    assert.equal(typeof actualResult.done, 'boolean', 'Incorrect iterator result: `done` is not boolean.')
  }

  if (expectedResult) {
    assert.deepEqual(actualResult, expectedResult, 'Incorrect iterator result.')
  }
}

/**
 * Checks if the object implements the async iterable protocol.
 *
 * @param {Object} obj
 * @returns {Promise}
 */
export async function assertIterableAsync (obj) {
  assert.equal(typeof obj[Symbol.asyncIterator], 'function', 'Is not iterable: has no `[Symbol.asyncIterator]()` method.')
}

/**
 * Checks if the object implements the async iterator protocol.
 *
 * @param {Object} obj
 * @returns {Promise}
 */
export async function assertIteratorAsync (...args) {
  return await assertIteratorSync(...args)
}

/**
 * Checks if the object implements the async iterable iterator protocol.
 *
 * @param {Object} obj
 * @returns {Promise}
 */
export async function assertIterableIteratorAsync (obj) {
  await assertIterableAsync(obj)
  await assertIteratorAsync(obj)

  const iterable = obj
  const iterator = obj[Symbol.asyncIterator]()

  assert.equal(iterable, iterator, 'Is not iterable iterator: `iterable !== iterable[Symbol.asyncIterator]()`.')
}

/**
 * Checks if the object is a valid iterator result.
 *
 * @param {Object} actualResult
 * @param {Object} [expectedResult]
 * @returns {Promise}
 */
export async function assertIteratorResultAsync (...args) {
  return await assertIteratorResultSync(...args)
}

/**
 * An alias for `assertIterableSync()`.
 *
 * @see {@link module:test-util/assert-iteration-protocols.assertIterableSync assertIterableSync}
 */
export function assertIterable (...args) {
  return assertIterableSync(...args)
}

/**
 * An alias for `assertIteratorSync()`.
 *
 * @see {@link module:test-util/assert-iteration-protocols.assertIteratorSync assertIteratorSync}
 */
export function assertIterator (...args) {
  return assertIteratorSync(...args)
}

/**
 * An alias for `assertIterableIteratorSync()`.
 *
 * @see {@link module:test-util/assert-iteration-protocols.assertIterableIteratorSync assertIterableIteratorSync}
 */
export function assertIterableIterator (...args) {
  return assertIterableIteratorSync(...args)
}

/**
 * An alias for `assertIteratorResultSync()`.
 *
 * @see {@link module:test-util/assert-iteration-protocols.assertIteratorResultSync assertIteratorResultSync}
 */
export function assertIteratorResult (...args) {
  return assertIteratorResultSync(...args)
}

export default {
  assertIterable,
  assertIterableIterator,
  assertIterator,
  assertIteratorResult,

  assertIterableAsync,
  assertIterableIteratorAsync,
  assertIteratorAsync,
  assertIteratorResultAsync,

  assertIterableSync,
  assertIterableIteratorSync,
  assertIteratorSync,
  assertIteratorResultSync
}
