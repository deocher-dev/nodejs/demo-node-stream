/**
 * This module contains frequently used assertion functions.
 *
 * @module
 */

import assert from 'node:assert/strict'

/**
 * Check if the value is an object.
 *
 * @throws {AssertionError}
 * @param {any} value
 */
export function assertObject (value) {
  assert.notEqual(value, null, 'Is not an object.')
  assert.equal(typeof value, 'object', 'Is not an object.')
}

export default {
  assertObject
}
