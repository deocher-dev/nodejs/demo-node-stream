import { Writable } from 'node:stream'

const SYMBOL_CONSTRUCT = Symbol.for('DemoWritable._construct')
const SYMBOL_DESTROY = Symbol.for('DemoWritable._destroy')
const SYMBOL_FINAL = Symbol.for('DemoWritable._final')

const DEFAULT_AUTO_DESTROY = true
const DEFAULT_DECODE_STRINGS = false // Do not convert strings to buffers.
const DEFAULT_EMIT_CLOSE = true
const DEFAULT_ENCODING = 'utf8'
const DEFAULT_HIGH_WATER_MARK = 1 // Amount of bytes or objects.
const DEFAULT_OBJECT_MODE = false

const WELL_KNOWN_EVENTS = ['close', 'drain', 'error', 'finish', 'pipe', 'unpipe']

/**
 * The demo stream, extends the `Writable` class. The class is designed to
 * provide chunks of data to the data store.
 *
 * The data store must implement `push()` method which accepts one or more
 * chunks of data. Here a chunk is an object with two properties:
 * - `chunk` - the data itself
 * - `encoding` - the encoding of the data
 *
 * While it is not necessary, the data store may implement "DemoWritable"
 * protocol as well. The protocol implies that a data store has next methods:
 * - `[Symbol.for('DemoWritable._construct')]()` - is responsible to initialize
 *   the data store before the stream starts to use it
 * - `[Symbol.for('DemoWritable._destroy')]()` - is responsible to destroy
 *   the data store
 * - `[Symbol.for('DemoWritable._final')]()` - is called after the last chunk
 *   of data has been pushed into the data store
 *
 * These methods will be called by the DemoWritable instance within its
 * `_construct()`, `_destroy()` and `_final()` methods respectively.
 *
 * @extends Writable
 *
 * @example
 * const dataStore = new DemoDataStoreAsync()
 * const writable = new DemoWritable(dataStore)
 *
 * // Will push chunks to the data store.
 * writable.write('1')
 * writable.write('2')
 * writable.end('3')
 */
export class DemoWritable extends Writable {
  _dataStore = null

  /**
   * @param {Object} dataStore
   * @param {Object} options Options of the `Writable` stream.
   * @param {boolean} [options.autoDestroy=true]
   * @param {boolean} [options.decodeStrings=false] Do not convert strings to buffers.
   * @param {(string|null)} [options.defaultEncoding='utf8']
   * @param {boolean} [options.emitClose=true]
   * @param {number} [options.highWaterMark=1] Amount of bytes or objects.
   * @param {boolean} [options.objectMode=false]
   */
  constructor (dataStore, options = {}) {
    const autoDestroy = 'autoDestroy' in options
      ? options.autoDestroy
      : DEFAULT_AUTO_DESTROY

    const decodeStrings = 'decodeStrings' in options
      ? options.decodeStrings
      : DEFAULT_DECODE_STRINGS

    const defaultEncoding = 'defaultEncoding' in options
      ? options.defaultEncoding
      : DEFAULT_ENCODING

    const emitClose = 'emitClose' in options
      ? options.emitClose
      : DEFAULT_EMIT_CLOSE

    const highWaterMark = 'highWaterMark' in options
      ? options.highWaterMark
      : DEFAULT_HIGH_WATER_MARK

    const objectMode = 'objectMode' in options
      ? options.objectMode
      : DEFAULT_OBJECT_MODE

    super({
      autoDestroy,
      decodeStrings,
      defaultEncoding,
      emitClose,
      highWaterMark,
      objectMode
    })

    this._dataStore = dataStore
  }

  /**
   * Wraps the original `emit()` method. Adds logging of the well known events.
   *
   * @ignore
   */
  emit (eventName, ...args) {
    /*
     * There are non documented events that are used by writable streams
     * internally. Print log for the documented events only, not for the
     * internal ones.
     */
    if (WELL_KNOWN_EVENTS.includes(eventName)) {
      console.debug('DemoWritable.emit: "%s"', eventName)
    }

    return super.emit(eventName, ...args)
  }

  /**
   * Returns the instance of the data store which was provided to the
   * constructor.
   *
   * @returns {Object}
   */
  getDataStore () {
    return this._dataStore
  }

  async _construct (callback) {
    try {
      console.debug('DemoWritable._construct')

      if (typeof this._dataStore[SYMBOL_CONSTRUCT] === 'function') {
        await this._dataStore[SYMBOL_CONSTRUCT]()
      }

      callback()
    } catch (err) {
      callback(err)
    }
  }

  async _write (chunk, encoding, callback) {
    try {
      console.debug('DemoWritable._write: chunk: "%s" (encoding: %s)', chunk, encoding)

      await this._dataStore.push({ chunk, encoding })

      callback()
    } catch (err) {
      callback(err)
    }
  }

  async _writev (chunks, callback) {
    try {
      console.debug('DemoWritable._writev: chunks: %d', chunks.length)

      await this._dataStore.push(...chunks)

      callback()
    } catch (err) {
      callback(err)
    }
  }

  async _destroy (err, callback) {
    try {
      console.debug('DemoWritable._destroy')

      if (typeof this._dataStore[SYMBOL_DESTROY] === 'function') {
        await this._dataStore[SYMBOL_DESTROY](err)
      }

      callback(err)
    } catch (err2) {
      callback(err2 ?? err)
    }
  }

  async _final (callback) {
    try {
      console.debug('DemoWritable._final')

      if (typeof this._dataStore[SYMBOL_FINAL] === 'function') {
        await this._dataStore[SYMBOL_FINAL]()
      }

      callback()
    } catch (err) {
      callback(err)
    }
  }
}

export default DemoWritable
