/**
 * The chunk of data.
 *
 * @typedef {any} Chunk
 */

/**
 * The chunk of data with encoding.
 *
 * @typedef {Object} ChunkItem
 * @property {Chunk} chunk
 * @property {(string|null)} encoding
 */
