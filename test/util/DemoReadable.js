import { Readable } from 'node:stream'

const SYMBOL_CONSTRUCT = Symbol.for('DemoReadable._construct')
const SYMBOL_DESTROY = Symbol.for('DemoReadable._destroy')

const DEFAULT_AUTO_DESTROY = true
const DEFAULT_EMIT_CLOSE = true
const DEFAULT_ENCODING = null
const DEFAULT_HIGH_WATER_MARK = 1 // Amount of bytes, characters or objects.
const DEFAULT_OBJECT_MODE = false

const WELL_KNOWN_EVENTS = ['close', 'data', 'end', 'error', 'pause', 'readable', 'resume']

/**
 * The demo stream, extends the `Readable` class. The class is designed to
 * consume chunks of data provided by the data source.
 *
 * The data source must be an iterator or async iterator, i.e. must implement
 * `next()` method which returns IteratorResult.
 *
 * While it is not necessary, the data source may implement "DemoReadable"
 * protocol as well. The protocol implies that a data source has next methods:
 * - `[Symbol.for('DemoReadable._construct')]()` - is responsible to initialize
 *   the data source before the stream starts to use it
 * - `[Symbol.for('DemoReadable._destroy')]()` - is responsible to destroy
 *   the data source
 *
 * These methods will be called by the DemoReadable instance within its
 * `_construct()` and `_destroy()` methods respectively.
 *
 * @extends Readable
 *
 * @example
 * const dataSource = new DemoDataSourceAsync(['1', '2', '3'])
 * const readable = new DemoReadable(dataSource, { encoding: 'utf8' })
 *
 * readable.resume() // Consume chunks from the data source, one by one.
 */
export class DemoReadable extends Readable {
  _dataSource = null

  /**
   * @param {Object} dataSource
   * @param {Object} options Options of the `Readable` stream.
   * @param {boolean} [options.autoDestroy=true]
   * @param {boolean} [options.emitClose=true]
   * @param {(string|null)} [options.encoding=null]
   * @param {number} [options.highWaterMark=1] Amount of bytes, characters or objects.
   * @param {boolean} [options.objectMode=false]
   */
  constructor (dataSource, options = {}) {
    const autoDestroy = 'autoDestroy' in options
      ? options.autoDestroy
      : DEFAULT_AUTO_DESTROY

    const emitClose = 'emitClose' in options
      ? options.emitClose
      : DEFAULT_EMIT_CLOSE

    const encoding = 'encoding' in options
      ? options.encoding
      : DEFAULT_ENCODING

    const highWaterMark = 'highWaterMark' in options
      ? options.highWaterMark
      : DEFAULT_HIGH_WATER_MARK

    const objectMode = 'objectMode' in options
      ? options.objectMode
      : DEFAULT_OBJECT_MODE

    super({
      autoDestroy,
      emitClose,
      encoding,
      highWaterMark,
      objectMode
    })

    this._dataSource = dataSource
  }

  /**
   * Wraps the original `emit()` method. Adds logging of the well known events.
   *
   * @ignore
   */
  emit (eventName, ...args) {
    /*
     * There are non documented events that are used by readable streams
     * internally. Print log for the documented events only, not for the
     * internal ones.
     */
    if (WELL_KNOWN_EVENTS.includes(eventName)) {
      console.debug('DemoReadable.emit: "%s"', eventName)
    }

    return super.emit(eventName, ...args)
  }

  /**
   * Returns the instance of the data source which was provided to the
   * constructor.
   *
   * @returns {Object}
   */
  getDataSource () {
    return this._dataSource
  }

  async _construct (callback) {
    try {
      console.debug('DemoReadable._construct')

      if (typeof this._dataSource[SYMBOL_CONSTRUCT] === 'function') {
        await this._dataSource[SYMBOL_CONSTRUCT]()
      }

      callback()
    } catch (err) {
      callback(err)
    }
  }

  async _read (size) {
    try {
      console.debug('DemoReadable._read: size: %d', size)

      const { done, value: chunk } = await this._dataSource.next()

      if (done) {
        this.push(null)
      } else {
        this.push(chunk)
      }
    } catch (err) {
      this.destroy(err)
    }
  }

  async _destroy (err, callback) {
    try {
      console.debug('DemoReadable._destroy')

      if (typeof this._dataSource[SYMBOL_DESTROY] === 'function') {
        await this._dataSource[SYMBOL_DESTROY](err)
      }

      callback(err)
    } catch (err2) {
      callback(err2 ?? err)
    }
  }
}

export default DemoReadable
