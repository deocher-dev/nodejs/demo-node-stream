import { setTimeout } from 'timers/promises'

import DemoDataStoreBase from './DemoDataStoreBase.js'

import '../typedefs.js'

const DEFAULT_DELAY_MS = 100

/**
 * The demo data store. Stores data as an array of chunks with its asynchronous
 * method `push()`.
 *
 * @extends module:test-util/DemoDataStore/DemoDataStoreBase.DemoDataStoreBase
 *
 * @example
 * const dataStore = new DemoDataStoreAsync()
 *
 * await dataStore.push(
 *   { chunk: '1', encoding: 'utf8' }
 * )
 * await dataStore.push(
 *   { chunk: '2', encoding: 'utf8' },
 *   { chunk: '3', encoding: 'utf8' }
 * )
 *
 * assert.deepEqual(dataStore.getChunks(), ['1', '2', '3'])
 */
export class DemoDataStoreAsync extends DemoDataStoreBase {
  _delay = DEFAULT_DELAY_MS

  /**
   * @param {Object} [options]
   * @param {number} [options.delay=200]
   *   The delay for async operations in milliseconds.
   * @param {boolean} [options.encodeBuffers=false]
   *   If true, the provided chunks will be converted to strings. Otherwise the
   *   type of the chunk will be preserved.
   * @param {number} [options.maxChunkLength=1024]
   *   The maximum length constraint for a chunk. The chunk that exceeds the
   *   provided constraint is considered as invalid.
   */
  constructor (options = {}) {
    const { delay = DEFAULT_DELAY_MS } = options

    super(options)

    this._delay = delay
  }

  /**
   * The method adds new chunk/chunks to the data store.
   *
   * @param {...ChunkItem} chunks
   * @returns {Promise}
   */
  async push (...chunks) {
    for (const { chunk, encoding } of chunks) {
      await this._storeChunk(chunk, encoding)
    }
  }

  async _storeChunk (chunk, encoding) {
    await setTimeout(this._delay)

    const encodedChunk = this.needEncoding(chunk)
      ? chunk.toString(encoding)
      : chunk

    if (!this.isValidChunk(encodedChunk)) {
      throw new Error('Invalid chunk.')
    }

    this._chunks.push(encodedChunk)
  }
}

export default DemoDataStoreAsync
