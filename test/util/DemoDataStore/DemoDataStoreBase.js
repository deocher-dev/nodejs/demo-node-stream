import '../typedefs.js'

const DEFAULT_ENCODE_BUFFERS = false // Do not convert buffers to strings.
const DEFAULT_MAX_CHUNK_LENGTH = 1024

/**
 * The base class for demo data stores. Descendants of this class are dedicated
 * to be a storage of data for writable streams.
 *
 * The class implements symbols:
 * - `[Symbol.for('DemoWritable._construct')]()`
 * - `[Symbol.for('DemoWritable._destroy')]()`
 * - `[Symbol.for('DemoWritable._final')]()`
 *
 * These methods should be called within `_construct()`, `_destroy()` and
 * `_final()` methods of the `Writable` stream respectively.
 *
 * @see {@link module:test-util/DemoWritable DemoWritable}
 */
export class DemoDataStoreBase {
  _chunks = []
  _isFinished = false

  _encodeBuffers = DEFAULT_ENCODE_BUFFERS
  _maxChunkLength = DEFAULT_MAX_CHUNK_LENGTH

  /**
   * @param {Object} [options]
   * @param {boolean} [options.encodeBuffers=false]
   *   If true, the provided chunks will be converted to strings. Otherwise the
   *   type of the chunk will be preserved.
   * @param {number} [options.maxChunkLength=1024]
   *   The maximum length constraint for a chunk. The chunk that exceeds the
   *   provided constraint is considered as invalid.
   */
  constructor (options = {}) {
    const {
      encodeBuffers = DEFAULT_ENCODE_BUFFERS,
      maxChunkLength = DEFAULT_MAX_CHUNK_LENGTH
    } = options

    if (!Number.isInteger(maxChunkLength)) {
      throw new Error('Invalid `maxChunkLength` option.')
    }
    if (maxChunkLength < 0) {
      throw new Error('Invalid `maxChunkLength` option.')
    }

    this._encodeBuffers = encodeBuffers
    this._maxChunkLength = maxChunkLength

    this.init()
  }

  [Symbol.for('DemoWritable._construct')] () {
    this.init()
  }

  [Symbol.for('DemoWritable._destroy')] (err) {
    if (err) {
      this.reset()
    }
  }

  [Symbol.for('DemoWritable._final')] () {
    this.finish()
  }

  /**
   * Init/reinit the state of the data store.
   */
  init () {
    this.reset()
  }

  /**
   * Reset the state of the data store.
   */
  reset () {
    this._chunks = []
    this._isFinished = false
  }

  /**
   * This method is abstract. Should be defined by descendants.
   *
   * @throws {Error} 'Abstract method not implemented.'
   *
   * @abstract
   */
  push () {
    throw new Error('Abstract method not implemented.')
  }

  /**
   * Mark the data store as finished. The method should be called by the
   * provider of data in order to report that there are no more chunks to push.
   */
  finish () {
    this._isFinished = true
  }

  /**
   * Returns stored chunks.
   *
   * @returns {Chunk[]}
   */
  getChunks () {
    return structuredClone(this._chunks)
  }

  /**
   * Get the value of `options.maxChunkLength` provided to the constructor.
   *
   * @returns {number}
   */
  getMaxChunkLength () {
    return this._maxChunkLength
  }

  /**
   * Check if the data store is marked as finished.
   *
   * @returns {boolean}
   */
  isFinished () {
    return this._isFinished
  }

  /**
   * Check if the chunk is valid.
   *
   * @param {Chunk} chunk
   * @returns {boolean}
   */
  isValidChunk (chunk) {
    if (typeof chunk === 'string' || Buffer.isBuffer(chunk)) {
      return chunk.length < this._maxChunkLength
    }

    return true
  }

  /**
   * Check if the chunk need to be converted into a string.
   *
   * @param {Chunk} chunk
   * @returns {boolean}
   */
  needEncoding (chunk) {
    return this._encodeBuffers && Buffer.isBuffer(chunk)
  }
}

export default DemoDataStoreBase
