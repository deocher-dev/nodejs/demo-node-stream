import DemoDataStoreBase from './DemoDataStoreBase.js'

import '../typedefs.js'

/**
 * The demo data store. Stores data as an array of chunks with its synchronous
 * method `push()`.
 *
 * @extends module:test-util/DemoDataStore/DemoDataStoreBase.DemoDataStoreBase
 *
 * @example
 * const dataStore = new DemoDataStoreSync()
 *
 * dataStore.push(
 *   { chunk: '1', encoding: 'utf8' }
 * )
 * dataStore.push(
 *   { chunk: '2', encoding: 'utf8' },
 *   { chunk: '3', encoding: 'utf8' }
 * )
 *
 * assert.deepEqual(dataStore.getChunks(), ['1', '2', '3'])
 */
export class DemoDataStoreSync extends DemoDataStoreBase {
  /**
   * The method adds new chunk/chunks to the store.
   *
   * @param {...ChunkItem} chunks
   */
  push (...chunks) {
    for (const { chunk, encoding } of chunks) {
      this._storeChunk(chunk, encoding)
    }
  }

  _storeChunk (chunk, encoding) {
    const encodedChunk = this.needEncoding(chunk)
      ? chunk.toString(encoding)
      : chunk

    if (!this.isValidChunk(encodedChunk)) {
      throw new Error('Invalid chunk.')
    }

    this._chunks.push(encodedChunk)
  }
}

export default DemoDataStoreSync
