import assert from 'node:assert/strict'

import {
  describe,
  it
} from 'node:test'

import {
  assertIterableIteratorAsync,
  assertIteratorResult
} from './util/assert-iteration-protocols.js'

import {
  DemoDataSourceAsync
} from './util/DemoDataSource/DemoDataSourceAsync.js'

describe('#000 Async data source', () => {
  describe('#000.1 interface', () => {
    const dataSource = new DemoDataSourceAsync(['1'])

    it('#.1 implements iterable iterator', async ctx => {
      await assertIterableIteratorAsync(dataSource)
    })
  })

  describe('#000.2 `next()` method', () => {
    const dataSource = new DemoDataSourceAsync(['1', '2', '3'])

    it('#.1 returns iterator result for the first item', async ctx => {
      assertIteratorResult(await dataSource.next(), { done: false, value: '1' })
    })

    it('#.2 returns iterator result for the second item', async ctx => {
      assertIteratorResult(await dataSource.next(), { done: false, value: '2' })
    })

    it('#.3 returns iterator result for the last item', async ctx => {
      assertIteratorResult(await dataSource.next(), { done: false, value: '3' })
    })

    it('#.4 returns iterator result after the last item', async ctx => {
      assertIteratorResult(await dataSource.next(), { done: true, value: undefined })
    })
  })

  describe('#000.3 `return()` method', () => {
    const dataSource = new DemoDataSourceAsync(['1'])

    it('#.1 does not consume the next item', async ctx => {
      assert.equal(dataSource.getConsumed(), 0, 'Data source item was consumed.')

      assertIteratorResult(await dataSource.return(), { done: true, value: '1' })

      assert.equal(dataSource.getConsumed(), 0, 'Data source item was consumed.')

      assertIteratorResult(await dataSource.next(), { done: false, value: '1' })

      assert.equal(dataSource.getConsumed(), 1, 'Data source item was not consumed.')
    })
  })

  describe('#000.4 `throw()` method', () => {
    const dataSource = new DemoDataSourceAsync(['1'])

    it('#.1 does not consume the next item', async ctx => {
      assert.equal(dataSource.getConsumed(), 0, 'Data source item was consumed.')

      assertIteratorResult(await dataSource.throw(), { done: true, value: '1' })

      assert.equal(dataSource.getConsumed(), 0, 'Data source item was consumed.')

      assertIteratorResult(await dataSource.next(), { done: false, value: '1' })

      assert.equal(dataSource.getConsumed(), 1, 'Data source item was not consumed.')
    })
  })

  describe('#000.5 produces', () => {
    it('#.1 0 items when capacity is 0', async ctx => {
      await assertIterationsAsync(new DemoDataSourceAsync([]))
    })

    it('#.2 1 item when capacity is 1', async ctx => {
      await assertIterationsAsync(new DemoDataSourceAsync(['1']))
    })

    it('#.3 2 items when capacity is 2', async ctx => {
      await assertIterationsAsync(new DemoDataSourceAsync(['1', '2']))
    })

    async function assertIterationsAsync (dataSource) {
      let counter = 0

      for await (const value of dataSource) {
        counter++

        assert.equal(value, String(counter), 'Incorrect data source value.')
      }

      assert.equal(counter, dataSource.getCapacity(), 'Incorrect amount of data source items.')
    }
  })
})
